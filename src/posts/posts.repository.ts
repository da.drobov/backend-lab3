import { dbConnection } from '../database/database.config';
import { EntityRepository } from '../interfaces';
import { User } from '../users';
import { Post } from './post.entity';

/**
 * Тут все методы сами описываете, референс с UsersRepository
 */
class PostsRepository implements EntityRepository<Post> {}

export const postsRepository = new PostsRepository();
